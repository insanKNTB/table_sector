import MainTable from "containers/MainTable";

import {Provider} from "react-redux";
import { createBrowserHistory } from 'history';

import store from "store/configureStore";

export const history = createBrowserHistory();

function App() {
  return (
    <Provider store={store}>
      <MainTable/>
    </Provider>
  )
}

export default App;
